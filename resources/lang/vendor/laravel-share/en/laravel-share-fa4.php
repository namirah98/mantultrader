<?php

return [
    'facebook' => '<a href=":url" class="social-button :class" id=":id"><span class="fa fa-facebook"></span></a>',
    'twitter' => '<a href=":url" class="social-button :class" id=":id"><span class="fa fa-twitter"></span></a>',
    'gplus' => '<a href=":url" class="social-button :class" id=":id"><span class="fa fa-google-plus"></span></a>',
    'linkedin' => '<a href=":url" class="social-button :class" id=":id"><span class="fa fa-linkedin"></span></a>',
    'whatsapp' => '<a target="_blank" href=":url" class="social-button :class" id=":id"><span class="fa fa-whatsapp"></span></a>',
    'pinterest' => '<a href=":url" class="social-button :class" id=":id"><span class="fa fa-pinterest"></span></a>',
];
