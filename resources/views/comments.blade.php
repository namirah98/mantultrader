@foreach($comments as $comment)

<div class="display-comment" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
    <h4 style="color: aliceblue">{{ $comment->name }}</h4>
    <p>{{ $comment->body }}</p>
    <a href="" id="reply"></a>
    <form method="post" action="{{ route('comments.store') }}">
        @csrf
        <div class="form-group mb-1">
            <input type="text" placeholder="Your Name" name="name" class="form-control" required>
        </div>
        <div class="form-group">
            <input type="text" name="body" class="form-control" placeholder="Comment" required />
            <input type="hidden" name="artikel_id" value="{{ $artikel->id }}" />
            <input type="hidden" name="parent_id" value="{{ $comment->id }}" />
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-warning" value="Reply" />
        </div>
    </form>
    @include('comments', ['comments' => $comment->replies])
</div>

@endforeach
