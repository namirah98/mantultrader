<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Mantul Trader</title>
    <meta charset="UTF-8">
    <meta name="description" content="EndGam Gaming Magazine Template">
    <meta name="keywords" content="endGam,gGaming, magazine, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link href="img/mantulbiru.PNG" rel="shortcut icon" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">


    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" />
    <link rel="stylesheet" href="{{asset('css/animate.css')}}" />

    <!-- Main Stylesheets -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />


    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header section -->
    <header class="header-section">
        <div class="header-warp">
            <div class="header-social d-flex justify-content-end">
                <p>Follow us:</p>
                <!-- <a href="#"><i class="fa fa-pinterest"></i></a> -->
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
                <a href="#"><i class="fa fa-youtube"></i></a>
                <!-- <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a> -->
            </div>
            <div class="header-bar-warp d-flex">
                <!-- site logo -->
                <a href="/home" class="site-logo">
                    <img src="{{ asset('img').'/'.$logo[0]->image}}" alt="">

                </a>
                <nav class="top-nav-area w-100">
                    @guest
                    <div class="user-panel">
                        <a href="/login">Login</a> / <a href="/register">Register</a>
                    </div>
                    @else
                    <div class="user-panel">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
												 document.getElementById('logout-form').submit();" style="color: blueviolet">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                    @endif
                    <!-- Menu -->
                    <ul class="main-menu primary-menu">
                        <li><a href="{{url('/')}}">Home</a></li>
                        
                        <li>
                            <a href="{{url('/blog')}}"">Blog</a>
							<ul class=" sub-menu">
                                <li><a href="game-single.html">Mantul Strategi</a></li>
                                <li><a href="contact.html">...</a>
                            </ul>
                        </li>
                    <li><a href="#">Reviews</a></li>
                    
                    <li>
                        <a href="#">Contact</a>
                            <ul class=" sub-menu">
                             <li align="center" class="fa fa-envelope" aria-hidden="true"> : info@mantultrader.id</li><br>
                             <li align="center" class="fa fa-phone-square" aria-hidden="true"> : 021 - 5225631</li>
                            </ul>
                    </li>
                    <!-- <li><a href="{{url('/blog')}}">Blog</a></li> -->
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <!-- Header section end -->


    <!-- content section -->
    @yield("content")
    <!-- end content section -->

    <!-- Newsletter section -->
    <section class="newsletter-section">
        <div class="container">
            <h2>Subscribe to our newsletter</h2>
            <form class="newsletter-form">
                <input type="text" placeholder="ENTER YOUR E-MAIL">
                <button class="site-btn">subscribe <img src="img/icons/double-arrow.png" alt="#" /></button>
            </form>
        </div>
    </section>
    <!-- Newsletter section end -->


    <!-- Footer section -->
    <footer class="footer-section">
        <div class="container">
            <div class="footer-left-pic">
                <img src="" alt="">
            </div>
            <div class="footer-right-pic">
                <img src="" alt="">
            </div>
            <a href="#" class="footer-logo">
                <img src="{{asset('img').'/'.$logo[0]->image}}" alt="">
            </a>
            
            <div class="footer-social d-flex justify-content-center">
                <!-- <a href="#"><i class="fa fa-pinterest"></i></a> -->
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
                <a href="#"><i class="fa fa-youtube"></i></a>
                <!-- <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a> -->
            </div>
            <div class="copyright"><a href="">MantulTrader</a> 2019</div>
        </div>
    </footer>
    <!-- Footer section end -->


    <!--====== Javascripts & Jquery ======-->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/jquery.sticky-sidebar.min.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/share.js')}}"></script>
</body>

</html>
