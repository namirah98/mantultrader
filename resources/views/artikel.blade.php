@extends("layout")
@section("content")

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="img/page-top-bg/1.jpg">
    <div class="page-info">
        <h2>MantulTrader</h2>
        <div class="site-breadcrumb">
            <a href="/">Home</a> /
            <span>MantulTrader</span>
        </div>
    </div>
</section>
<!-- Page top end-->


<!-- Games section -->
<section class="games-single-page">
    <div class="container">
        <div class="game-single-preview">
            <img src="img/games/big.jpg" alt="">
        </div>
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7 game-single-content">
                <div class="gs-meta">11.11.18 / in <a href="">Games</a></div>
                <h2 class="gs-title">Final Appocalipse 2.1</h2>
                <h4>Gameplay</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquamet, consectetur
                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vestibulum
                    posuere porttitor justo id pellentesque. Proin id lacus feugiat, posuere erat sit amet, commodo
                    ipsum. Donec pellentesque vestibulum metus.</p>
                <h4>Conclusion</h4>
                <p>Nulla ut maximus mauris. Sed malesuada at sapien sed euismod. Vestibulum pharetra in sem id laoreet.
                    Cras metus ex, placerat nec justo quis, luctus posuere ex. Vivamus volutpat nibh ac sollicitudin
                    imperdiet. Donec scelerisque lorem sodales odio ultricies, nec rhoncus ex lobortis. Vivamus
                    tincidunt sit amet sem id varius. Donec ele-mentum aliquet tortor. Curabitur justo mi, efficitur sed
                    eros aliquet, dictum molestie eros. Nullam scelerisque convallis gravida. Morbi id lorem accumsan,
                    scelerisque enim laoreet, sollicitudin neque. Vivamus volutpat nibh ac sollicitudin imperdiet. Donec
                    scelerisque lorem sodales odio ultricies, nec rhoncus ex lobortis. Vivamus tincidunt sit amet sem id
                    varius. Donec ele-mentum aliquet tortor. Curabitur justo mi, efficitur sed eros aliqueDonec vitae
                    tellus sodales, congue augue at, biben-dum justo. Pellentesque non dolor et magna volutpat pharetra
                    eget vel ligula. Maecenas facilisis vestibulum mattis. Sed sagittis gravida urna. Cras nec mi risus.
                </p>
                <div class="geme-social-share pt-5 d-flex">
                    <p>Share:</p>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-dribbble"></i></a>
                    <a href="#"><i class="fa fa-behance"></i></a>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 sidebar game-page-sideber">
                <div id="stickySidebar">
                    <div class="widget-item">
                        <div class="rating-widget">
                            
                            </div>
                        </div>
                    </div>
                    <div class="widget-item">
                        <div class="testimonials-widget">
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Games end-->

<section class="game-author-section">
    <div class="container">
        <div class="game-author-pic set-bg" data-setbg="img/author.jpg"></div>
        <div class="game-author-info">
            <h4>Written by: Michael Williams</h4>
            <p></p>
        </div>
    </div>
</section>
<!-- Coment section  -->

<!-- Coment section  -->
<section class="game-author-section">
    <div class="container">
        <div class="col-9">
           
            <hr />
            <div class="game-author-info">
                <h4>Add comment</h4>
            </div>
            <form method="post" action="{{ route('comments.store') }}">
                @csrf
                <div class="form-group">
                    <input type="text" placeholder="Your Name" name="name" class="form-control" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="body" placeholder="Comment" required></textarea>
                    <input type="hidden" name="" value="" />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn" style="background:#b01ba5; color: aliceblue" value="Add Comment" />
                </div>
            </form>
        </div>
    </div>
</section>
<!-- end  coment -->
<!-- end  coment -->
@endsection
