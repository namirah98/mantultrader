<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
@if(backpack_user()->hasRole('admin'))
<li class="treeview">
    <a href="#"><i class="fa fa-file-archive-o"></i> <span>Master</span> <i
            class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('category') }}"><i class="fa fa-building"></i> <span>Category</span></a></li>
    </ul>
</li>
<li><a href="{{ backpack_url('artikel') }}"><i class="fa fa-file"></i> <span>Artikel</span></a></li>
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users Management</span> <i
            class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('users') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
      <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
      <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-cog"></i> <span>Setting</span> <i
            class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('logo') }}"><i class="fa fa-image"></i> <span>Logo</span></a></li>
        <li><a href="{{ backpack_url('banner') }}"><i class="fa fa-camera"></i> <span>Banner</span></a></li>
        <!-- <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Jabatan</span></a></li> -->
    </ul>
</li>
@endif
@if(backpack_user()->hasRole('konten'))
<li><a href="{{ backpack_url('artikel') }}"><i class="fa fa-file"></i> <span>Artikel</span></a></li>
<li class="treeview">
    <a href="#"><i class="fa fa-cog"></i> <span>Setting</span> <i
            class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('banner') }}"><i class="fa fa-camera"></i> <span>Banner</span></a></li>
    </ul>
</li>
@endif