@extends("layout")
@section("content")

<!-- Hero section -->
<section class="hero-section overflow-hidden">
    <div class="hero-slider owl-carousel">
        @foreach($banner as $item)
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center"
            data-setbg="{{('img/').$item->image}}">
            <div class="container"> <br><br><br><br>
    
                <a href="#" class="site-btn">Read More <img src="img/icons/double-arrow.png" alt="#" /></a>
            </div>
        </div>
        @endforeach
    </div>
</section>
<!-- Hero section end-->


<!-- Intro section -->
<section class="intro-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="intro-text-box text-box text-white">
                    <div class="top-meta">{{$date[0]}}</div>
                    <h3>{{$title[0]}}</h3>
                    <p>{!!str_limit($isi[0],200)!!}....</p>
                    <a href="{{$link[0]}}" class="read-more">Read More <img src="img/icons/double-arrow.png" alt="#" /></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="intro-text-box text-box text-white">
                    <div class="top-meta">{{$date[1]}} </div>
                    <h3>{{$title[1]}}</h3>
                    <p>{!!str_limit($isi[1], 200)!!}........</p>
                    <a href="{{$link[1]}}" class="read-more">Read More <img src="img/icons/double-arrow.png" alt="#" /></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="intro-text-box text-box text-white">
                    <div class="top-meta">{{$date[2]}} </div>
                    <h3>{{$title[2]}}</h3>
                    <p>{!!str_limit($isi[2],200)!!}....</p>
                    <a href="{{$link[2]}}" class="read-more">Read More <img src="img/icons/double-arrow.png" alt="#" /></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Intro section end -->


<!-- Blog section -->
<section class="blog-section spad">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <div class="section-title text-white">
                    <h2>Latest News</h2>
                </div>
                
                <!-- Blog item -->
                @foreach($artikel as $key => $item)
                <div class="blog-item">
                    <div class="blog-thumb">
                        <img src="{{ asset('post-img').'/'.$item->image}}" alt="">
                    </div>
                    <div class="blog-text text-box text-white">
                        <div class="top-meta">{{$date[$key]}}</div>
                        <h3>{{$title[$key]}}</h3>
                        <p>{!!str_limit($isi[$key], 256)!!}........</p>
                        <a href="{{$link[$key]}}" class="read-more">Read More <img src="img/icons/double-arrow.png" alt="#" /></a>
                    </div>
                </div>
                @endforeach
            </div>
            
        </div>
    </div>
</section>
<!-- Blog section end -->


<!-- Intro section -->
<section class="intro-video-section set-bg d-flex align-items-end " data-setbg="./img/promo-bg.jpg">
    <a href="https://www.youtube.com/watch?v=uFsGy5x_fyQ" class="video-play-btn video-popup"><img
            src="img/icons/solid-right-arrow.png" alt="#"></a>
    <div class="container">
        <div class="video-text">
            <h2>MANTULTRADER</h2>
            <p>Buying Low And Selling High.</p>
        </div>
    </div>
</section>
<!-- Intro section end -->


<!-- Featured section -->
<section class="featured-section">
    <div class="featured-bg set-bg" data-setbg="img/giphy.gif"></div>
    <div class="featured-box">
        <div class="text-box">
            <div class="top-meta"> <a href=""></a></div>
            <h2>Beli dengan harga rendah dan jual dengan harga tinggi.</h2>
            <p>
            Halo Sobat Mantul semuanya!Pasti penasaran dong sama Mantul Trader.
            Mamen akan bantu Sobat sekalian yang ingin menjadi trader yang keren dan bisa cuan banyak. 
            wiiih tunggu apalagi? Yuk ikuti jejak Mamen yaaa... SEE YOU! </p>
            <a href="#" class="read-more">Read More <img src="img/icons/double-arrow.png" alt="#" /></a>
        </div>
    </div>
</section>
<!-- Featured section end-->
@endsection
