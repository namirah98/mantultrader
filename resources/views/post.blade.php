@extends("layout")
@section("content")

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="{{asset('img/page-top-bg/1.jpg')}}">
    <div class="page-info">
        <h2>MantulTrader</h2>
        <div class="site-breadcrumb">
            <a href="/">Home</a> /
            <span>MantulTrader</span>
        </div>
    </div>
</section>
<!-- Page top end-->


<!-- Games section -->
<section class="games-single-page">
    <div class="container">
        <div class="game-single-preview">
            <img src=" {{ asset('post-img').'/'.$artikel->image}}" alt="">
        </div>
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7 game-single-content">
                <div class="gs-meta">{{$date}} / in <a href="">MantulTrader</a></div>
                <h2 class="gs-title">{{$artikel->title}}</h2>
                <p style="color: white !important">{!! $artikel->body !!}</p>
                <div class="geme-social-share pt-5 d-flex">
                    <p>Share:</p>
                    {!!$share!!}
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 sidebar game-page-sideber">
                <div id="stickySidebar">
                    <div class="widget-item">
                        <div class="rating-widget">
                            <!-- <h4 class="widget-title">Ratings</h4>
                            <ul>
                                <li>Price<span>3.5/5</span></li>
                                <li>Graphics<span>4.5/5</span></li>
                                <li>Levels<span>3.5/5</span></li>
                                <li>Levels<span>4.5/5</span></li>
                                <li>Dificulty<span>4.5/5</span></li>
                            </ul>
                            <div class="rating">
                                <h5><i>Rating</i><span>4.5</span> / 5</h5>
                            </div> -->
                        </div>
                    </div>
                    <div class="widget-item">
                        <div class="testimonials-widget">
                            <!-- <h4 class="widget-title">Testimonials</h4>
                            <div class="testim-text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolo re magna aliqua. Quis ipsum suspend isse ultrices.</p>
                                <h6><span>James Smith,</span>Gamer</h6>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Games end-->

<section class="game-author-section">
    <div class="container">
        <div class="game-author-pic set-bg" data-setbg="img/author.jpg"></div>
        <div class="game-author-info">
            <h4>Written by: {{$write[0]->name}}</h4>
            
        </div>
    </div>
</section>
<!-- Coment section  -->
<section class="game-author-section">
    <div class="container">
        <div class="col-9">
            @include('comments', ['comments' => $artikel->comments, 'post_id' => $artikel->id])
            <hr />
            <div class="game-author-info">
                <h4>Add comment</h4>
            </div>
            <form method="post" action="{{ route('comments.store') }}">
                @csrf
                <div class="form-group">
                    <input type="text" placeholder="Your Name" name="name" class="form-control" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="body" placeholder="Comment" required></textarea>
                    <input type="hidden" name="artikel_id" value="{{ $artikel->id }}" />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn" style="background:#b01ba5; color: aliceblue" value="Add Comment" />
                </div>
            </form>
        </div>
    </div>
</section>
<!-- end  coment -->
@endsection
