<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    public function __construct(array $attributes = [])
    {
        $this->creating([$this, 'onCreating']);

        parent::__construct($attributes);
    }

    public function onCreating(\App\Models\Artikel $row)
    {
        if (!\backpack_auth()->user()->id) {
            return false;
        }

        $row->setAttribute('user_id', \backpack_auth()->user()->id);
    }

    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */

    protected $table = 'artikel';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['user_id', 'category_id', 'slug', 'title', 'body', 'image', 'approve'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */
    public function setImageAttribute($value)
    {
        // $attribute_name = "image";
        // $disk = "public";
        // $destination_path = public_path('/img');

        // $image = \Image::make($value);
        // $filename = md5($value.time()).'.jpg';
        // \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
        // $this->attribute[$attribute_name] = /*$destination_path.'/'.*/$filename;

        // $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        // $timestamp = $this->getTimeNow();
        // $file = Input::file('image');

        //     //rename to all smallcap
        // $ori_filename = $file->getClientOriginalName();

        // $name = $ori_filename;
        //     // $question->photo      = $name;
        // $file->move(public_path().'/img', $name);

        // $image = $value;
        // // dd($image);
        // if ($image == 'avatar.png') {
        //     $attribute_name = "image";
        //     $this->attributes['image'] = $image;
        // } else {
        //     $input['image'] = time() . '.' . $image->getClientOriginalExtension();
        //     $img = \Image::make($image->getRealPath());

        //     $destinationPath = public_path('/img');
        //     $img->resize(750, 450, function ($constraint) {
        //         $constraint->aspectRatio();
        //     })->save($destinationPath . '/' . $input['image']);

        //     $destinationPath = public_path('/img');

        //     $img->resize(100, 100, function ($constraint) {
        //         $constraint->aspectRatio();
        //     })->save($destinationPath . '/' . $input['image']);

        //     // $image->move($destinationPath, $input['imagename']); // for no resize
        //     $this->attributes['image'] = strtolower($input['image']);
        // }

        $image = $value;
        // dd($image);
        $input['image'] = time() . '.' . $image->getClientOriginalExtension();
        $img = \Image::make($image->getRealPath());

        $destinationPath = public_path('post-img');
        $img->save($destinationPath . '/' . $input['image']);

        $destinationPath = public_path('post-img');

        $img->save($destinationPath . '/' . $input['image']);
        $this->attributes['image'] = strtolower($input['image']);

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */
    public function categoryId()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->whereNull('parent_id');
    }
}
