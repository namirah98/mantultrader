<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    // public function __construct(array $attributes = [])
    // {
    //     $this->creating([$this, 'onCreating']);

    //     parent::__construct($attributes);
    // }

    // public function onCreating(\App\Models\Comment $row)
    // {
    //     if (!\backpack_auth()->user()->id) {
    //         return false;
    //     }

    //     $row->setAttribute('user_id', \backpack_auth()->user()->id);
    // }

    protected $fillable = ['name', 'artikel_id', 'parent_id', 'body'];

    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }
}
