<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArtikelRequest as StoreRequest;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ArtikelRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use App\Models\Artikel;

/**
 * Class ArtikelCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ArtikelCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\Artikel');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/artikel');
        $this->crud->setEntityNameStrings('artikel', 'artikels');

        if(backpack_user()->hasRole('admin')){
            $this->crud->removeAllButtons();
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
         */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        $this->crud->setColumns([
            [
                'label' => 'Kategori',
                'type' => 'select',
                'name' => 'category_id', 
                'entity' => 'categoryId', 
                'attribute' => 'name', 
                'model' => 'App\Models\Category',
            ],
            [
                'name' => 'title',
                'label' => 'Title',
                'type' => 'text',
            ],
            [
                'name' => 'body',
                'label' => 'Content',
                'type' => 'text',
            ],
            [
                'name' => 'image', // The db column name
                'label' => "Image", // Table column heading
                'type' => 'image',
                'prefix' => 'post-img/',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Kategori',
                'type' => 'select',
                'name' => 'category_id', 
                'entity' => 'categoryId', 
                'attribute' => 'name', 
                'model' => 'App\Models\Category',
            ],
            [
                'name' => 'title',
                'label' => 'Title',
                'type' => 'text',
            ],
            [
                'name' => 'slug',
                'label' => 'Slug',
                'type' => 'text',
            ],
            [ 
                'name' => 'image',
                'label' => 'Image',
                'type' => 'upload',
                'upload' => true,
                'crop' => true,
                'prefix' => 'post-img/',
                'disk' => 'public',
            ],
            [   
                'name' => 'body',
                'label' => 'Content',
                'type' => 'summernote'
            ],
        ]);
        $this->crud->orderBy('created_at','DESC');

        // add asterisk for fields that are required in ArtikelRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
        // $idUser= \backpack_auth()->user()->id;
        // $artikel = new Artikel;
        // $artikel->user_id                = $idUser;
        // $artikel->category_id            = $request->input('category_id');
        // $artikel->slug                   = $request->input('slug');
        // $artikel->title                  = $request->input('title');
        // $artikel->body                   = $request->input('body');
        // $artikel->image                  = $request->input('image');
        // $artikel->save();
        // return redirect('/admin/artikel');
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
