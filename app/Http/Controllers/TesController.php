<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use App\Models\Banner;
use App\Models\Logo;
use Carbon\Carbon;
use DB;
use Jorenvh\Share\ShareFacade as Share;

class TesController extends Controller
{
    public function index()
    {
        $logo = Logo::all();
        $banner = Banner::all();
        $artikel = Artikel::latest("created_at")->orderBy('created_at', 'desc')->take(3)->get();
        $categ = DB::table('artikel')
            ->join('category', 'category.id', '=', 'artikel.category_id')
            ->select('category.name')
            ->latest("artikel.created_at")
            ->orderBy('artikel.created_at', 'desc')
            ->take(3)
            ->get();
        $acateg = json_decode($categ, true);
        $cate = array();
        foreach ($acateg as $key => $value) {
            $cate[] = preg_replace('/\s+/', '', strtolower($value['name']));
        }
        foreach ($artikel as $key => $value) {
            $title[] = $value['title'];
            $isi[] = strip_tags($value['body']);
            $link[] = $cate[$key] . '/' . $value['slug'];
            $image[] = $value['image'];
            $date[] = Carbon::parse($value['created_at'], 'Asia/Jakarta')->formatLocalized('%A, %d %B %Y');
        }
        return view('home', compact('artikel', 'logo', 'title', 'isi', 'link', 'image', 'date', 'banner'));
    }

    public function blog()
    {
        $logo = Logo::all();
        return view('blog', compact('logo'));
    }

    public function artikel()
    {
        $logo = Logo::all();
        return view('artikel', compact('logo'));
    }

    public function post($catname, $slug)
    {
        if (!$slug) {
            return redirect('404');
        }
        $artikel = Artikel::where('slug', $slug)->first();
        $write = DB::table('artikel')
            ->join('users', 'artikel.user_id', '=', 'users.id')
            ->where('artikel.slug', '=', $slug)
            ->select('users.name')
            ->get();
        $date = Carbon::parse($artikel['created_at'], 'Asia/Jakarta')->formatLocalized('%A, %d %B %Y');
        $logo = Logo::all();
        $share = Share::page(url()->current(), $artikel->title)->facebook()->twitter()->whatsapp();
        return view('post', compact('logo', 'artikel', 'date', 'share', 'write'));
    }

    public function shared(Request $request)
    {

        $inputs = $request->all();
        dd($inputs);

        $id = $inputs['contentId'];
        $shareType = $inputs['shareType'];
        $post = Posts::findOrFail($id);

        if (!isset($shareType)) {
            $shareType = 'facebook';
        }

        if (null == Cookie::get('BuzzyPostshared' . $shareType . $post->id)) {
            cookie('BuzzyPostshared' . $shareType . $post->id, $post->id, 15000, makeposturl($post));
        } else {
            return "ok";
        }

        $pshared = [];
        $oshared = $post->shared;

        if (isset($post->shared->facebook)) {
            $pshared['facebook'] = $shareType == 'facebook' ? $oshared->facebook + 1 : $oshared->facebook;
        } else {
            $pshared['facebook'] = $shareType == 'facebook' ? 1 : 0;
        }

        $post->shared = json_encode($pshared);
        $post->save();

        return "ok";

    }

}
