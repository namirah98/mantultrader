<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'TesController@index');
Route::get('/blog', 'TesController@blog');
Route::get('/artikel', 'TesController@artikel');

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function () {
    CRUD::resource('category', 'Admin\CategoryCrudController');
    CRUD::resource('artikel', 'Admin\ArtikelCrudController');
    CRUD::resource('users', 'Admin\UsersCrudController');
    CRUD::resource('logo', 'Admin\LogoCrudController');
    CRUD::resource('banner', 'Admin\BannerCrudController');
});
Route::group(['prefix' => 'admin', 'middleware' => ['role:konten|admin']], function () {
    CRUD::resource('artikel', 'Admin\ArtikelCrudController');
    CRUD::resource('banner', 'Admin\BannerCrudController');
});

Route::get('/{catname}/{slug}', 'TesController@post');
Route::post('/comments', 'CommentsController@store')->name('comments.store');
Route::post('shared', 'TesController@shared');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
