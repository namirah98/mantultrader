<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config = config('permission.table_names');

        DB::table($config['roles'])->insert(array(
            array('name' => 'admin', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
            array('name' => 'konten', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
                array('name' => 'komentar', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
        ));
    }
}
