<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = array(
            array('Trading Guides'),
            array('Price Action'),
            array('Chart Patterns'),
            array('Chandlestick Patterns'),
            array('Trading Indicators'),
            array('Trading Tips'),
        );

        $categorycount = count($category);

        for ($i = 0; $i < $categorycount; $i++) {
            DB::table('category')->insert(array(
                'name' => $category[$i][0],
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ));
        }
    }
}
