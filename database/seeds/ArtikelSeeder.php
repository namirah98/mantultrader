<?php

use Illuminate\Database\Seeder;

class ArtikelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artikel')->insert(array(
            array(
                'user_id' => '1',
                'category_id' => '1',
                'slug' => 'test',
                'title' => 'Ini Test',
                'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius-mod tempor incididunt ut
                labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Lorem ipsum dolor
                sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua</p>',
                'image' => 'test.jpg',
                'approve' => '1',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'user_id' => '1',
                'category_id' => '1',
                'slug' => 'test1',
                'title' => 'Ini Test 1',
                'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius-mod tempor incididunt ut
                labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Lorem ipsum dolor
                sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua</p>',
                'image' => 'test.jpg',
                'approve' => '1',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'user_id' => '1',
                'category_id' => '1',
                'slug' => 'test2',
                'title' => 'Ini Test 2',
                'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius-mod tempor incididunt ut
                labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Lorem ipsum dolor
                sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua</p>',
                'image' => 'test.jpg',
                'approve' => '1',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        ));
    }
}
