<?php

use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banner')->insert(array(
            array(
                'image' => 'slider-bg-1.jpg',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'image' => 'slider-bg-2.jpg',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        ));
    }
}
