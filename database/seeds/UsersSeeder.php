<?php

use Illuminate\Database\Seeder;
use App\Models\BackpackUser as User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'Administrator',
                'email' => 'admin@database.com',
                'password' => Hash::make('123456'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
        ]);
        $user1->assignRole('admin');
        $user2 = User::create([
            'name' => 'Admin Konten',
                'email' => 'admin@konten.com',
                'password' => Hash::make('123456'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
        ]);
        $user2->assignRole('konten');
        $user3 = User::create([
            'name' => 'Admin Komentar',
                'email' => 'admin@komentar.com',
                'password' => Hash::make('123456'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
        ]);
        $user3->assignRole('komentar');
    }
}
