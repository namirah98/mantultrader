<?php

use Illuminate\Database\Seeder;

class LogoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logo')->insert(array(
            array(
                'image' => 'mantulbiru.PNG',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        ));
    }
}
